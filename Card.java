public class Card{
	private String suit;
	private String value;
	
	public Card(String suit, String value){
		this.suit = suit;
		this.value = value;
	}
	
	public String getSuit(){
		return this.suit;
	}
	
	public String getValue(){
		return this.value;
	}
	
	public String toString(){
		return this.value + " of " + this.suit;
	}
	
	public double calculateScore(){
		
		double valueScore = 0.0; 
		double suitScore = 0.0; 
		
		if(this.value.equals("Ace")){
			valueScore =+  1.0;
		}
		
		if(this.value.equals("Two")){
			valueScore =+  2.0;
		}
		
		if(this.value.equals("Three")){
			valueScore =+  3.0;
		}
		
		if(this.value.equals("Four")){
			valueScore =+  4.0;
		}
		
		if(this.value.equals("Five")){
			valueScore =+  5.0;
		}
		
		if(this.value.equals("Six")){
			valueScore =+  6.0;
		}
		
		if(this.value.equals("Seven")){
			valueScore =+  7.0;
		}
		
		if(this.value.equals("Eight")){
			valueScore =+  8.0;
		}
		
		if(this.value.equals("Nine")){
			valueScore =+  9.0;
		}
		
		if(this.value.equals("Ten")){
			valueScore =+  10.0;
		}
		
		if(this.value.equals("Jack")){
			valueScore =+  11.0;
		}
		
		if(this.value.equals("Queen")){
			valueScore =+  12.0;
		}
		
		if(this.value.equals("King")){
			valueScore =+  13.0;
		}
		
		if(this.suit.equals("Hearts")){
			suitScore =+  0.4;
		}
		
		if(this.suit.equals("Spades")){
			suitScore =+  0.3;
		}
		
		if(this.suit.equals("Diamonds")){
			suitScore =+  0.2;
		}
		
		if(this.suit.equals("Clubs")){
			suitScore =+  0.1;
		}
		return valueScore + suitScore;
	}
}