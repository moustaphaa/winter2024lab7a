import java.util.Random;

public class Deck {
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	public Deck() {
		this.rng = new Random();
		this.cards = new Card[52];
		this.numberOfCards = 52;
		String[] suits = new String[] {"Spades", "Clubs", "Hearts", "Diamonds"};
		String[] value = new String[] {"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
		
		int count = 0;
		for(int i = 0; i < suits.length; i++){
			for(int j = 0; j < value.length; j++){
				this.cards[count] = new Card(suits[i], value[j]);
				count = count + 1;
			}
		}
	}
	
	public int length(){
		return this.numberOfCards;
	}
	
	public Card drawTopCard(){
		this.numberOfCards --;
		return cards[numberOfCards];
		
	}
	
	public String toString(){
		String result = "";
		for(int i = 0; i < numberOfCards; i++){
			result = result + this.cards[i].toString() + "\n";
		}
		return result;
	}
	
	public void shuffle(){
		for(int i = 0; i < numberOfCards; i++){
			
			int newPosition = rng.nextInt(numberOfCards);
			Card temp = this.cards[i];
			this.cards[i] = this.cards[newPosition];
			this.cards[newPosition] = temp;
		}
	}
}