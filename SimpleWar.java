public class SimpleWar{
	public static void main(String[] args){
		Deck myDeck = new Deck();
		myDeck.shuffle();
		
		
		int player1 = 0;
		int player2 = 0;
		
		while(myDeck.length() > 0){
			
			Card player1Card = myDeck.drawTopCard();
			System.out.println("Player 1's card is: " + player1Card);
			System.out.println("Player 1's score is: " + player1Card.calculateScore());
			
			Card player2Card = myDeck.drawTopCard();
			System.out.println("Player 2's card is: " + player2Card);
			System.out.println("Player 2's score is: " + player2Card.calculateScore());
		
			
			if(player1Card.calculateScore() > player2Card.calculateScore()){
				System.out.println("Player 1 wins this round!");
				player1 = player1 + 1;
				System.out.println("---------------------------------");
			}
			else {
				System.out.println("Player 2 wins this round!");
				player2 = player2 + 1;
				System.out.println("---------------------------------");
			}
		}
		
		if(player1 > player2){
			System.out.println("Congratulations player 1! You won with " + player1 + " points. It's okay player 2, you still got " + player2 + " points.");
		}
		if(player1 < player2){
			System.out.println("Congratulations player 2! You won with " + player2 + " points. It's okay player 1, you still got " + player1 + " points.");
		}
		else{
			System.out.println("It's a tie! Good job to both players");
		}
	}
}